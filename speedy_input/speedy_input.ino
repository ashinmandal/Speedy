/*
  Arduino Controller / Listener for Speedy
 */
 
const int tiltPin = 11;

void setup() {
  // initialize the serial communication:
  Serial.begin(9600);
  pinMode(tiltPin, INPUT);
}

void loop() {
  // send the value to Processing:
  Serial.println((String)digitalRead(tiltPin));
}
