import processing.serial.*;
import ddf.minim.*;
import ddf.minim.ugens.*;

// INITIALISING VARIABLES

// The serial port
Serial myPort;

Minim minim;
AudioOutput out;
AudioPlayer bgMusic;
Oscil fm;

int time, value, prevValue, speed;
int wait = 20;
PImage[] allFramesWithBlur = new PImage[120];
PImage[] allFramesWithNoBlur = new PImage[120];
PImage[] allFramesWithWarp = new PImage[120];
int i;
int current = 0;

void setup(){
  size(640, 480);
  for(i=0; i < 120; i++) {
    String formattedNumber = String.format("%04d", i);
    allFramesWithBlur[i] = loadImage("/blur/" + formattedNumber + ".jpg");
    allFramesWithNoBlur[i] = loadImage("/noblur/" + formattedNumber + ".jpg");
    formattedNumber = String.format("0_%05d", i);
    allFramesWithWarp[i] = loadImage("/warp/" + formattedNumber + ".jpg");
  } 
  time = millis();//store the current time
  
  // Serial Port Initialisation for Windows
  myPort = new Serial(this, "COM7", 9600);
  
  // Serial Port Initialisation for MAC
  //myPort = new Serial(this, "/dev/tty.usbmodemfd121", 9600);

  myPort.bufferUntil('\n');
  prevValue = value;
  speed = 0;
  
  // initialize the minim and out objects
  minim = new Minim( this );
  
  // background music starts playing
  bgMusic = minim.loadFile("bg.mp3");
  bgMusic.loop();
  
  
  out   = minim.getLineOut();
  Oscil wave = new Oscil( 200, 0.8, Waves.TRIANGLE );
  fm   = new Oscil( 10, 2, Waves.SINE );
  // set the offset of fm so that it generates values centered around 200 Hz
  fm.offset.setLastValue( 200 );
  // patch it to the frequency of wave so it controls it
  fm.patch( wave.frequency );
  // and patch wave to the output
  wave.patch( out );
  fm.setAmplitude(200.0);
  out.setVolume(0.05);
  
}
void draw(){
  background(0);
  if (prevValue != value) {
    if (speed < 500) 
      speed+= 25;
    else
      speed = 500;
  } else {
    if (speed > 5)
      speed = speed - 5;
    else 
      speed = 0;
  } 
  
  println("prevValue: " + prevValue + " value: " + value + " Speed is: " + speed);
  if (speed < 400) {
    image(allFramesWithNoBlur[current], 0, 0);
    current++;
    if (current > 119) 
      current = 0;
    frameRate(map(speed, 0, 400, 15, 60));
    //println("mouseY: " + mouseY + " at Animation 1.");
  } else if (speed >= 400 && speed < 470 ) {
    image(allFramesWithBlur[current], 0, 0);
    current++;
    if (current > 119) 
      current = 0;
    frameRate(60);
    //println("mouseY: " + mouseY + " at Animation 2.");
  } else {
//    if(millis() - time >= wait){
//      image(allFramesWithBlur[current], 0, 0);
//      time = millis();//also update the stored time
//    }
    image(allFramesWithWarp[current], 0, 0);
    current++;
    if (current > 119) 
      current = 0;
    frameRate(60);
    //println("mouseY: " + mouseY + " at Animation 3.");
  }
  prevValue = value;
  fm.setFrequency(map(speed, 0, 400, 80, 300));
}

// Listening to the Serial Port and getting 
// ASCII values - 48 = 0;  49 = 1
void serialEvent (Serial myPort) {
  // get the Serial inString sent from Arduino if the port is available:
  if (myPort.available() > 0) {
    String inString = myPort.readString();
    //println(inString);
    value = (int)inString.charAt(0);
    //println("ASCII Value: " + value);
  }
}
